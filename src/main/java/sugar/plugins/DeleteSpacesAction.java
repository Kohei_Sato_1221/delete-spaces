/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sugar.plugins;

import com.atlassian.confluence.security.GateKeeper;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.util.longrunning.LongRunningTaskId;
import com.atlassian.confluence.util.longrunning.LongRunningTaskManager;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import com.opensymphony.webwork.ServletActionContext;
import javax.servlet.ServletContext;
import static org.apache.commons.lang.StringUtils.isNotBlank;
import static org.apache.commons.lang.StringUtils.trim;
import static com.atlassian.confluence.security.SpacePermission.ADMINISTER_SPACE_PERMISSION;
import com.atlassian.confluence.spaces.actions.AbstractSpaceAction;
import com.opensymphony.webwork.interceptor.ServletRequestAware;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.web.context.ServletContextAware;

/**
 *
 * @author KoheiSato
 */
public class DeleteSpacesAction extends AbstractSpaceAction implements ServletContextAware, ServletRequestAware{
//    extends AbstractSpaceAction implements ServletContextAware, ServletRequestAware
    private SpaceManager spaceManager;
    private LongRunningTaskManager longRunningTaskManager;
    private LongRunningTaskId taskId;
    private TransactionTemplate transactionTemplate;
    private boolean synchronous;
    private ServletContext servletContext;
    private HttpServletRequest servletRequest;
    private GateKeeper gateKeeper;

    
    
    @Override
    public String execute() throws Exception{
        DeleteSpacesTask dTask = new DeleteSpacesTask(getAuthenticatedUser(), gateKeeper, transactionTemplate, getSpaceManager());
        if(isLongRunningTaskSupported() && !isSynchronous()){
            taskId = longRunningTaskManager.startLongRunningTask(getAuthenticatedUser(), dTask);
        }else{
            dTask.run();
        }
        return SUCCESS;
    }
    
  
    private boolean isLongRunningTaskSupported(){
        final String runningServer = getServletContext().getServerInfo().toLowerCase();
        final String unsupportedContainers = getServletContext().getInitParameter("unsupportedContainersForExportLongRunningTask");

        if(isNotBlank(unsupportedContainers)){
            final String[] containers = unsupportedContainers.split(",");
            for (String container1 : containers) {
                final String container = trim(container1);
                if(runningServer.contains(container)){
                    return false;
                }
            }
        }
        return true;
    }
    
    @Override
    public void validate(){
        super.validate();
    }
 
    @Override
    protected List getPermissionTypes(){
        List permissionTypes = super.getPermissionTypes();
        addPermissionTypeTo(ADMINISTER_SPACE_PERMISSION, permissionTypes);
        return permissionTypes;
    }
 
    public ServletContext getServletContext(){
        if (servletContext != null)
            return servletContext;
        else
            return ServletActionContext.getServletContext();
    }

    public void setServletContext(ServletContext servletContext){
        this.servletContext = servletContext;
//        setApplicationContext(WebApplicationContextUtils.getWebApplicationContext(servletContext));
    }
    
    public String getTaskId(){
        return taskId.toString();
    }

    public void setTransactionTemplate(TransactionTemplate tt){
        this.transactionTemplate = tt;
    }
    
    /**
     * @return the spaceManager
     */
    public SpaceManager getSpaceManager() {
	return spaceManager;
    }

    /**
     * @param spaceManager the spaceManager to set
     */
    public void setSpaceManager(SpaceManager spaceManager) {
	this.spaceManager = spaceManager;
    }

    /**
     * @param longRunningTaskManager the longRunningTaskManager to set
     */
    public void setLongRunningTaskManager(LongRunningTaskManager longRunningTaskManager) {
        this.longRunningTaskManager = longRunningTaskManager;
    }
    
    public boolean isSynchronous()
    {
        return synchronous;
    }

    public void setSynchronous(boolean synchronous)
    {
        this.synchronous = synchronous;
    }
 
    public void setGateKeeper(GateKeeper gateKeeper){
        this.gateKeeper = gateKeeper;
    }

    @Override
    public void setServletRequest(HttpServletRequest hsr) {
        this.servletRequest = servletRequest;
    }

}
