/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sugar.plugins;

import com.atlassian.confluence.security.GateKeeper;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.spaces.SpaceStatus;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.confluence.util.longrunning.ConfluenceAbstractLongRunningTask;
import com.atlassian.sal.api.transaction.TransactionCallback;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import com.atlassian.confluence.pages.PageManager;
import java.util.List;
import java.util.Random;

/**
 *
 * @author KoheiSato
 */
public class DeleteSpacesTask extends ConfluenceAbstractLongRunningTask{
   private TransactionTemplate transactionTemplate;
   private SpaceManager spaceManager;
   private ConfluenceUser user;
   private GateKeeper gateKeeper;
   private List<String> selectedSpaceKeys;
   private String deletedSpaceKey;
   
   public DeleteSpacesTask(final ConfluenceUser user, GateKeeper gateKeeper, TransactionTemplate transactionTemplate, SpaceManager spaceManager){
       this.user = user;
       this.gateKeeper = gateKeeper;
       this.transactionTemplate = transactionTemplate;
       this.spaceManager = spaceManager;
   }
   
   public DeleteSpacesTask(final ConfluenceUser user, GateKeeper gateKeeper, TransactionTemplate transactionTemplate, SpaceManager spaceManager, List<String> selectedSpaceKeys ){
       this(user, gateKeeper, transactionTemplate, spaceManager);
       this.selectedSpaceKeys = selectedSpaceKeys;
   }

    @Override
    protected void runInternal() {
        progress.setStatus("Preparing Transaction....");
        AuthenticatedUserThreadLocal.set(user);
        SpaceManager tempSpManager = getSpaceManager();
        if(selectedSpaceKeys.size() <= 0){
            selectedSpaceKeys = (List<String>) tempSpManager.getAllSpaceKeys(SpaceStatus.CURRENT);
        }
        int number_of_spaces = selectedSpaceKeys.size();
        int percentageOfOneSpace = 100 / number_of_spaces;
        int progressTotal = 0;
        progress.setPercentage(0);
        while(true){
            deletedSpaceKey = selectedSpaceKeys.get(new Random().nextInt(selectedSpaceKeys.size()));
            progressTotal += percentageOfOneSpace; 
            progress.setPercentage(progressTotal);
            transactionTemplate.execute(new TransactionCallback<Object>(){
                @Override
                public Object doInTransaction() {
                    progress.setStatus("Starting Transaction!!!");
                    deleteSpace(deletedSpaceKey);
                    return "SUCCESS";
                }
            });
        if(tempSpManager.getAllSpaceKeys(SpaceStatus.CURRENT).size() <= 0){
            break;
        }
        }

        progress.setStatus("Space Deletion Completed!! " + number_of_spaces + " spaces Deleted!!");
        progress.setPercentage(100);
        log.info("Space Delettion completed!!");
    }

    private void deleteSpace(String spaceKey){
        SpaceManager tempSpManager = getSpaceManager();
        progress.setStatus("Now Deleting space : " + spaceKey);
        tempSpManager.removeSpace(tempSpManager.getSpace(spaceKey));
        progress.setStatus("Deleted space : " + spaceKey);
    }

    @Override
    public String getName() {
        return "Delete Spaces";
    }

    public void setGateKeeper(GateKeeper gateKeeper) {
        this.gateKeeper = gateKeeper;
    }
    
    public void setTransactionTemplate(TransactionTemplate tt){
        this.transactionTemplate = tt;
    }
    
    /**
     * @return the spaceManager
     */
    public SpaceManager getSpaceManager() {
	return spaceManager;
    }

    /**
     * @param spaceManager the spaceManager to set
     */
    public void setSpaceManager(SpaceManager spaceManager) {
	this.spaceManager = spaceManager;
    }

    /**
     * @return the selectedSpaceKeys
     */
    public List<String> getSelectedSpaceKeys() {
        return selectedSpaceKeys;
    }

    /**
     * @param selectedSpaceKeys the selectedSpaceKeys to set
     */
    public void setSelectedSpaceKeys(List<String> selectedSpaceKeys) {
        this.selectedSpaceKeys = selectedSpaceKeys;
    }
    
}
