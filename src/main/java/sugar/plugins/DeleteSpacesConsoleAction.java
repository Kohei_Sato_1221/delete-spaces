package sugar.plugins;

import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.confluence.security.GateKeeper;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.spaces.SpaceStatus;
import com.atlassian.confluence.util.longrunning.LongRunningTaskId;
import com.atlassian.confluence.util.longrunning.LongRunningTaskManager;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import com.opensymphony.webwork.ServletActionContext;
import com.opensymphony.webwork.interceptor.ServletRequestAware;
import static com.opensymphony.xwork.Action.SUCCESS;
import com.opensymphony.xwork.ActionContext;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import static org.apache.commons.lang.StringUtils.isNotBlank;
import static org.apache.commons.lang.StringUtils.trim;
import org.springframework.web.context.ServletContextAware;

/**
 *
 * @author KoheiSato
 */
public class DeleteSpacesConsoleAction extends ConfluenceActionSupport  implements ServletContextAware, ServletRequestAware{
   private String strListSpace;
   private SpaceManager spaceManager;
   private LongRunningTaskManager longRunningTaskManager;
   private LongRunningTaskId taskId;
   private TransactionTemplate transactionTemplate;
   private boolean synchronous;
   private ServletContext servletContext;
   private HttpServletRequest servletRequest;
   private GateKeeper gateKeeper;

    @Override
    public String execute() throws Exception{
        setSpaceListToHiddenParaeter();
        return SUCCESS;
    }
    
    public String doDelete() throws Exception{
        List<String> selectedSpaceKeys = getSelectedSpaceKeys();
        DeleteSpacesTask dTask = new DeleteSpacesTask(getAuthenticatedUser(), gateKeeper, transactionTemplate, getSpaceManager(),selectedSpaceKeys);
        if(isLongRunningTaskSupported() && !isSynchronous()){
            taskId = longRunningTaskManager.startLongRunningTask(getAuthenticatedUser(), dTask);
        }else{
            dTask.run();
        }
        return SUCCESS;
    }
    
    private void setSpaceListToHiddenParaeter(){
        SpaceManager tempSpManager = getSpaceManager();
        List<String> allSpaces = (List<String>) tempSpManager.getAllSpaceKeys(SpaceStatus.CURRENT);
        StringBuilder sb = new StringBuilder();
        sb.append("<thead>");
        sb.append("<tr>");
        sb.append("<td align=\"left\"><input type=\"checkbox\" class=\"kodcheckspace\" name=\"all\" ></td>");
        sb.append("<td>");
        sb.append("SpaceKey(").append(allSpaces.size()).append(")");
        sb.append("</td>");
        sb.append("</tr>");
        sb.append("</thead>");
        sb.append("<tbody>");
        
        for(String spacekey : allSpaces){
            sb.append("<tr>");
            sb.append("<td align=\"left\"><input type=\"checkbox\" class=\"kodcheck").append(spacekey).append("\" name=\"kodcheck").append(spacekey).append("\"></td>");
            sb.append("<td>").append(spacekey).append("</td>");
            sb.append("</td>");
            sb.append("</tr>");
        }

        
        sb.append("</tbody>");
        setStrListSpace(sb.toString());
    }
    
    private List<String> getSelectedSpaceKeys(){
        List<String> selectedSpaceKeys = new ArrayList<String>();
        ActionContext actionContext = ActionContext.getContext();
        SpaceManager tempSpManager = getSpaceManager();
        List<String> listSpace = (List<String>) tempSpManager.getAllSpaceKeys(SpaceStatus.CURRENT);
        // すべてのスペースに対してチェックされたかどうかを調べる
        for (int index1 = 0; index1 < listSpace.size(); index1++) {
            String spaceKey = listSpace.get(index1);
            boolean isSelected = getFirstCheckBoxParameterValueAsBoolean(actionContext, "kodcheck" + spaceKey);
            selectedSpaceKeys.add(spaceKey);
            if (isSelected) {
                selectedSpaceKeys.add(spaceKey);
            }
        }
        return selectedSpaceKeys;
    }

     private boolean getFirstCheckBoxParameterValueAsBoolean(ActionContext context, String key) {
        String value = getFirstParameterValueAsString(context, key);
        return value != null && value.equalsIgnoreCase("on");
    }
     
    private String getFirstParameterValueAsString(ActionContext context, String key) {
        Object paramValues = context.getParameters().get(key);
        if (paramValues instanceof String[] && ((String[]) paramValues).length != 0) {
            return ((String[]) paramValues)[0];
        } else if (paramValues instanceof String) {
            return (String) paramValues;
        }
        return null;
    }
    
     private boolean isLongRunningTaskSupported(){
        final String runningServer = getServletContext().getServerInfo().toLowerCase();
        final String unsupportedContainers = getServletContext().getInitParameter("unsupportedContainersForExportLongRunningTask");

        if(isNotBlank(unsupportedContainers)){
            final String[] containers = unsupportedContainers.split(",");
            for (String container1 : containers) {
                final String container = trim(container1);
                if(runningServer.contains(container)){
                    return false;
                }
            }
        }
        return true;
    }
    
    
    /**
     * @return the strListSpace
     */
    public String getStrListSpace() {
        return strListSpace;
    }

    /**
     * @param strListSpace the strListSpace to set
     */
    public void setStrListSpace(String strListSpace) {
        this.strListSpace = strListSpace;
    }
    
    /**
     * @return the spaceManager
     */
    public SpaceManager getSpaceManager() {
	return spaceManager;
    }

    /**
     * @param spaceManager the spaceManager to set
     */
    public void setSpaceManager(SpaceManager spaceManager) {
	this.spaceManager = spaceManager;
    }
    
    public ServletContext getServletContext(){
        if (servletContext != null)
            return servletContext;
        else
            return ServletActionContext.getServletContext();
    }

    public void setServletContext(ServletContext servletContext){
        this.servletContext = servletContext;
    }
    
    public boolean isSynchronous()
    {
        return synchronous;
    }

    public void setSynchronous(boolean synchronous)
    {
        this.synchronous = synchronous;
    }
    
    public void setLongRunningTaskManager(LongRunningTaskManager longRunningTaskManager) {
        this.longRunningTaskManager = longRunningTaskManager;
    }

    public String getTaskId(){
        return taskId.toString();
    }
    
    public void setTransactionTemplate(TransactionTemplate tt){
        this.transactionTemplate = tt;
    }
    
     public void setGateKeeper(GateKeeper gateKeeper){
        this.gateKeeper = gateKeeper;
    }
    
    public void setServletRequest(HttpServletRequest hsr) {
        this.servletRequest = servletRequest;
    }
}